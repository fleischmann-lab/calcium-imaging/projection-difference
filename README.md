# Piriform Projection Differences

[[_TOC_]]

## Quick links

- Repository: <https://gitlab.com/fleischmann-lab/calcium-imaging/projection-difference>
- Static site (for better viewing of notebooks): <https://fleischmann-lab.gitlab.io/calcium-imaging/projection-difference>
- DANDI Archive (embargoed): <https://dandiarchive.org/dandiset/000785>


## Description

This repository is part of an ongoing research project in the Fleischmann lab at Brown University, led by Dr. Simon Daste.
The goal of the project is to analyze neural activity of different projection pyramidal neurons in the piriform cortex (PCx), specifically:

- **OB-p**: PCx neurons that project to the olfactory bulb (OB)
- **mPFC-p**: PCx neurons that project to the medial prefrontal cortex (mPFC)

This repository describes how to access and preprocess NWB data generated from the experiments of the project.
It is made to share data for collaboration purposes. 
Scripts and notebooks for analysis of these data are currently in a different repository.

If you have questions, feedback or need access to data, either raise an issue or contact us.


## Requirements

The general requirements are:

- `python3` (preferably `3.9, 3.10, 3.11`)
- Jupyter Lab (preferable) or Jupyter Notebook
- DANDI Archive account API key (see `Data & Acess` section below)

Optionally, you may also want / need:

- [`zstd`](https://facebook.github.io/zstd/) (for compression)


## Data & Access

The data are currently on [DANDI Archive](https://dandiarchive.org/) (embargoed) and will need an API key to download. 

If you have not been granted access, please create an account on DANDI Archive with your GitHub account and contact us with your email or GitHub username.

If this is your first time using `dandi` with an API key, it will likely prompt you to set up a keyring to store your API key.

For more information on access credentials, please see [DANDI Archive handbook](https://docs.dandiarchive.org/13_upload/#storing-access-credentials).


## Collaboration

### Color schemes

For consistency of visualization, please refer to `config/graphics.yml` file for the main colors.

#### Projection / population

This file defines the color schemes for the different **projection targets** / populations, specifically, colors related to:

- <font color="#00AEEF">**OB**</font> should be variants / shadings of `#00AEEF` (sky blue)
- <font color="#EC008C">**mPFC**</font> should be variants / shadings of `#EC008C` (pink)
- non-specific (<font color="#262b33">*any*, *untagged*, *unknown*</font>) ~ greys / blacks

Note: this color scheme is colorblind accessible (by testing in browser colorblind emulation).

#### Other variables

For everything else, it's **up to you**. We can all discuss them later if needed.

On our end, we typically choose palettes from [colorbrewer](https://colorbrewer2.org/), e.g.:

- `RdBu` for diverging
- `Greys` for sequential
- `Paired` for phases.

You don't have to follow these choices, as they may be subject to change.

Regardless of choices, the schemes should **prioritize colorblind accessibility**.

If you have questions or issues, please let us know.


### Code repositories

As of now, this repository is only for sharing, preprocessing and documenting the DANDI dataset, as well as highlighting the different considerations and nuances about the experiments and processed data.

For collaboration purposes, we all work on different parts and utilize different techniques to analyze the data.
Hence, currently there is no need to share and collaborate on codes for analyses.

We will leave it up to you to decide what's best for you and your analysis. We will revisit matters about sharing codes for analysis / modelling once we collectively feel they are ready.

If / when you wish to discuss these matters, please let us know.


## Steps

### 1. Set up

First clone the repository:

```bash
git clone https://gitlab.com/fleischmann-lab/calcium-imaging/projection-difference
cd projection-difference
```

This demonstrates setting up a virtual environment with `venv`.

```bash
# create environment
python -m venv .venv

# activate environment
source .venv/bin/activate

# install requirements
pip install -U pip
pip install -r requirements.txt
```

Since Jupyter Notebooks can be run with different setups (installed in the same enviroment, called from a different kernel, Google Collab, ...), we will leave it to you to decide how you want to peruse Jupyter Lab / Notebook.

Alternatively to `venv`, you can choose to use `conda` environments via Anaconda, Miniconda, Mamba, ...

Note: you may occasionally need to upgrade `dandi` and `dandischema` packages to download from DANDI.

If you run into issues, please let us know.


### 2. Download data

#### 2.1. Initial download

When you first clone the repository, the `data/dandi` will be empty.

Before downloading, please read the `Data & Access` section to obtain access.

To download, do this on your command line:

```bash
dandi download --output-dir data/dandi/ "https://dandiarchive.org/dandiset/000785/"
```

This will then download into `data/dandi/000785` within 5-10 minutes, depending on your internet connection. 

Currently, the entire dandiset is ~7 GB.


#### 2.2. Download updated data

If you have already downloaded this before in the same `data/dandi` folder, add the `--existing` tag appriopriately (see [documentation](https://dandi.readthedocs.io/en/latest/cmdline/download.html#cmdoption-e)).

For example, if you want overwriting existing files, do:

```bash
dandi download --existing overwrite --output-dir data/dandi/ "https://dandiarchive.org/dandiset/000785/"
```


### 3. Construct metadata database

For quick querying/selection of experiments, please run the [`construct-metadata-database.ipynb`](notebooks/construct-metadata-database.ipynb) notebook.

This will save a CSV file with metadata at `data/database.csv`.

This is necessary to combine and process multiple files of the same experiment.

Please refer to the following for information about the metadata file.


#### 3.1. Experiments

The column `experiment_name` will be either one of these:

| Name | Imaging Rate | Session Activity | Description | Note |
| --: | :--: | :--: | :-- | :-- |
| `identity_dataset` | ~4.5 Hz / plane | passive only | **identity** dataset with 8 odorants and 2 controls | - |
| `concentration_dataset` | ~4.5 Hz per plane | passive only | **concentration** dataset with 3 odorants across 3 concentrations, and 1 control | - |
| `fast_hz_sniff_trig_dataset` | ~30 Hz per plane | passive only | **identity** dataset with 8 odorants and 2 controls, stim triggered with **sniff** <br>imaged for only 1 plane, **faster** rate than `identity_dataset` | note: this may need revisiting, don't use for now |
| `GNG_2021_dataset` | ~4.5 Hz per plane | 1 passive, and 3 GNG sessions | **Go/Nogo** dataset with 2 GO (CS+1, CS+2) and 2 NOGO stimuli (CS-1, CS-2) in 2021 | - |
| `GNG_2023b_dataset` | ~4.5 Hz per plane | 1 passive, and 4 GNG sessions | **Go/Nogo** dataset with 2 GO (CS+1, CS+2) and 2 NOGO stimuli (CS-1, CS-2) in 2023, <br>with some modifications of passive odor sets and task scripts | typically day 4 is ignored to be consistent with analysis for `GNG_2021_dataset` |


#### 3.2. Metadata Columns

The following is information about the columns in the metadata file:

| Name | Type | Description | Relevance | 
| --: | :--: | :-- | :-- |
| <font color=blue>**GERNAL METADATA**</font> | - | - | - |
| `file_path` | str | NWB file path | all datasets |
| `subject_id` | str / int | subject ID | all datasets | 
| `projection_area` | str | projection area tagged in the experiment with subject (**not** of a given cell) | all datasets |
| `recording_area` | str | area of recording (constant, `PCx`) | all datasets | 
| `date_id` | str | experiment date in `YYYYMMDD` | all datasets | 
| `date` | str / Datetime | actual datetime of experiment | all datasets |
| `day_number` | int / float | day number of GNG experiment  <br>note: relative to passive session, i.e. day 0; task days 1, 2, ... | `NaN` for non-GNG | 
| `experiment_name` | str | experiment name / dataset | all datasets | 
| `experiment_task` | str | experiment task (`passive`, `go_nogo`) | all datasets |
| `session_description` | str | label of experiment session, will be `experiment_name`, <br>concatenated with details about task/passive day number if GNG dataset | all datasets |
| `experiment_description` | str | more detailed description of experiment or session | all datasets |
| <font color=blue>**TRIAL & STIM METADATA**</font> | - | - | - |
| `num_stims` | int | number of unique stimuli, incl. control, in session | all datasets |
| `num_trials` | int | number of trials in experiment session | all datasets |
| `num_bad_trials` | int | number of bad trials in experiment session | all datasets |
| <font color=blue>**IMAGING METADATA**</font> | - | - | - |
| `num_planes` | int | number of imaging planes per experiment session | all datasets |
| `total_frames` | int | total number of frames in an imaging plane in the session | all datasets |
| `frames_per_trial` | int | number of calcium imaging frames per trial session | all datasets |
| `total_cells` | int | total number of ROIs (incl. actual cells and non cells) <br>across all planes in an experiment session | all datasets |
| `total_is_cells` | int | total number of cells (i.e. `is_cell` in suite2p) <br>across all planes in an experiment session | all datasets |
| `total_is_redcell` | int | total number of labeled cells (i.e. projection cells) <br>across all planes in an experiment session | all datasets |
| <font color=blue>**TASK METADATA**</font> | - | - | - |
| `percent_go_trial` | float | percent of GO trials in the GNG session | `NaN` for non-GNG datasets and passive sessions |
| `percent_nogo_trial` | float | percent of NOGO trials in the GNG session | `NaN` for non-GNG datasets and passive sessions |
| `percent_hit_trial` | float | percent of Hit trials (i.e. correct GO) in the GNG session | `NaN` for non-GNG datasets and passive sessions |
| `percent_miss_trial` | float | percent of Miss trials (i.e. incorrect GO) in the GNG session | `NaN` for non-GNG datasets and passive sessions |
| `percent_correction_rejection_trial` | float | percent of Correct Rejection trials (i.e. correct NOGO) in the GNG session | `NaN` for non-GNG datasets and passive sessions |
| `percent_false_alarm_trial` | float | percent of False Alarm trials (i.e. incorrect NOGO) in the GNG session | `NaN` for non-GNG datasets and passive sessions |
| `task_accuracy` | float | task percent accuracy of animal in GNG session | `NaN` for non-GNG datasets and passive sessions |
| `task_TPR` | float | task true positive rate in GNG session | `NaN` for non-GNG datasets and passive sessions |
| `task_TNR` | float | task true negative rate in GNG session | `NaN` for non-GNG datasets and passive sessions |
| `task_balanced_accuracy` | float | task balanced accruracy (i.e. mean of TPR and TNR) of GNG session | `NaN` for non-GNG datasets and passive sessions |


### 4. Process & integrate fluorescence data

#### 4.1. Passive datasets

After constructing the metadata database `data/database.csv`, please go to [`process-passive-dataset.ipynb`](notebooks/process-passive-dataset.ipynb) to integrate different NWB files and process fluoresence data from **passive** experiments.

Please note the scope and considerations laid out in the notebook.

The data will be saved in `data/proc/{EXPERIMENT_NAME}` folder.  

The following table describes the columns in the main dataframe generated (also in notebook):

| Name | Type | Description | Relevance | 
| --: | :--: | :-- | :-- |
| <font color=blue>**SESSION METADATA**</font> | - | - | - |
| `subject_id` | str / int | subject ID | all datasets | 
| `proj_area` | str | projection area tagged in the experiment with subject (**not** of a given cell) | all datasets | 
| `date_id` | str | experiment date in `YYYYMMDD` | all datasets | 
| `exp_id` | str | experiment session ID, combining info about the above 3 columns | all datasets |
| `num_redcells` | int | number of red tagged cells in the subject | all datasets |
| `experiment_name` | str | experiment name / dataset | all datasets | 
| `experiment_task` | str | experiment task (`passive`, `go_nogo`) | all datasets |
| `recording_area` | str | recording area (always `PCx`) | all datasets |
| <font color="blue">**TRIAL METADATA**</font> | - | - | - |
| `trial_id` | int | trial ID / order | all datasets |
| `is_bad_trial` | bool | indicates whether the trial has very extreme outliers | all datasets | 
| <font color="blue">**STIM METADATA**</font> | - | - | - |
| `stim_id` | int / str | unique stimulus ID of trial | all datasets |
| `stim_name` | str | stimulus chemical name in trial | all datastes |
| `stim_type` | str | whether `odor` or `control` stimulus | all datasets |
| `stim_conc` | str | concentration / intensity of odor (`low`, `medium`, `high`) | only `concentration_dataset` |
| `stim_class` | str | odor class, short alias `stim_name` (`A`, `B`, `C`) | only `concentration_dataset` |
| `stim_onset_frame` | int | frame index in `dFF0` that stimulus is ON | all datasets |
| `stim_offset_frame` | int | frame index in `dFF0` that stimulus is OFF | all datasets |
| <font color="blue">**CELL METADATA**</font> | - | - | - |
| `cell_id` | int | cell ID (combined across all planes, usually already filtered with suite2p `iscell`) | all datasets |  
| `is_redcell` | bool | whether this cell is tagged and indicates projects to `proj_area` | all datasets | 
| `projection` | str | summarize `is_redcell` and `proj_area` for a given cell | all datasets | 
| <font color="blue">**FLUORESCENCE DATA**</font> | - | - | - |
| `dFF0` | list / array | array of normalized fluorescence of a given cell of a given trial | all datasets |  
| `t` | list / array | array of time (seconds) to accompany `dFF0` vector, relative to start of trial | all datasets |  

#### 4.2. Go/NoGo (GNG) datasets

Please go to [`process-GNG-dataset.ipynb`](notebooks/process-GNG-dataset.ipynb) to integrate different NWB files and process fluoresence data from **GNG** datasets.

Please note the scope and considerations laid out in the notebook.

The data will be saved in `data/proc/GNG` folder.  

The following table describes the columns in the main fluorescence dataframe generated (also in notebook):

| Name | Type | Description | Relevance | 
| --: | :--: | :-- | :-- |
| <font color="blue">**SESSION METADATA**</blue> | - | - | - |
| `subject_id` | str / int | subject ID | all datasets | 
| `proj_area` | str | projection area tagged in the experiment with subject (**not** of a given cell) | all datasets | 
| `date_id` | str | experiment date in `YYYYMMDD` | all datasets | 
| `day_num` | int / float | day number of GNG experiment  <br>note: relative to passive session, i.e. day 0; task days 1, 2, ... | GNG datasets | 
| `exp_id` | str | experiment session ID, combining info about the above 3 columns | all datasets |
| `num_redcells` | int | number of red tagged cells in the subject | all datasets |
| `experiment_name` | str | experiment name / dataset | all datasets | 
| `experiment_task` | str | experiment task (`passive`, `go_nogo`) | all datasets |
| `recording_area` | str | recording area (always `PCx`) | all datasets |
| <font color="blue">**TRIAL METADATA**</blue> | - | - | - |
| `trial_id` | int | trial ID / order | all datasets |
| `is_bad_trial` | bool | indicates whether the trial has very extreme outliers | all datasets | 
| `trial_type` | str | trial type during passive session (`control`) or during GNG task (`NOGO` ~ `CS-` stim, `GO`, `CS+` stim) | GNG datasets | 
| `trial_type_id` | int | trial type ID as int (see table below for mapping)  | GNG datasets | 
| <font color="blue">**STIM METADATA**</blue> | - | - | - |
| `stim_id` | int / str | unique stimulus ID of trial | all datasets |
| `stim_name` | str | stimulus chemical name in trial | all datastes |
| `stim_type` | str | whether `odor` or `control` stimulus | all datasets |
| `stim_name_value` | str | stim name and "value" to be associated (`control`, `CS-1`, `CS+1`, `CS-2`, `CS+2`, `US2`, `US1`)<br>**note**: use this for analysis and focus on `CS[+/-][1/2]` stimuli; see table below for mapping | GNG datasets | 
| `stim_task_tag` | str | stim task tag (`control`, `pre-task`, `during-task`, `odor`, see table below for mapping) <br> essentially, this is to tag conditioned stimuli (CS) during passive (`pre-task`) and GNG session (`during-task`) | GNG datasets | 
| `stim_valence` | str | stim valence, i.e. reward association (`none` for `CS-`, `reward` for `CS+`, `control` for everything else) | GNG datasets | 
| `stim_valence_id` | int | stim valence ID as int (see table below for mapping) | GNG datasets | 
| ~~`stim_conc`~~ | ~~str~~ | ~~concentration / intensity of odor (`low`, `medium`, `high`)~~ | only `concentration_dataset` |
| ~~`stim_class`~~ | ~~str~~ | ~~odor class, short alias `stim_name` (`A`, `B`, `C`)~~ | only `concentration_dataset` |
| `stim_onset_frame` | int | frame index in `dFF0` that stimulus is ON<br>note 1: in GNG dataset, at ~5-sec mark from beginning of trial<br>note 2: we use `_frame` for stimulus on/offset instead of actual times for consistency with other datasets | all datasets |
| `stim_offset_frame` | int | frame index in `dFF0` that stimulus is OFF<br>note: in GNG dataset, at ~7-sec mark from beginning of trial| all datasets |
| <font color="blue">**BEHAVIOR METADATA**</blue> | - | - | - |
| `outcome` | str | outcome of the animal's behavior (`correct_rejection`, `hit`, `miss`, `false_alarm`), if control trial then `none` | GNG datasets | 
| `outcome_id` | int | outcome ID as int (see table below for mapping) | GNG datasets | 
| `has_reward` | bool | whether the animal received reward in the trial (only `hit` trial) | GNG datasets | 
| `action` | str | category of the animal's action (`correct` ~ Hit/CR, `incorrect` ~ Miss/FA, `none` for passive)<br>note: the naming could be better, essentially this says whether the animal *act*ed correctly or not | GNG datasets | 
| `action_id` | int | action ID as int (see table below for mapping) | GNG datasets |
| <font color="blue">**BEHAVIOR & TASK DATA**</blue> | - | - | - |
| `wait_for_lick_onset` | float | when the machine started considering when licks can achieve reward (i.e. end of wait / delay period)<br>note: in GNG dataset, at ~10-sec mark from beginning of trial, NaN for passive sessions | GNG datasets | 
| `reward_onset` | float | when the animal received reward after `wait_for_lick_onset`<br>note: in GNG dataset, only in `hit` trials, and where `has_reward=True`, NaN otherwise | GNG datasets | 
| `lick_onset` | list of float | list of animal lick onsets (relative to trial starts, only for GNG sessions)<br>note: there were rare occasion where licks were weirdly recorded, in which the lick duration was too quick (less than a few ms) or too long (more than 0.5 s); you can use `lick_duration` to filter these out if you analyze licks | GNG datasets | 
| `lick_duration` | list of float | list of duration of licks for each onset in `lick_onset` list (only for GNG sessions) | GNG datasets | 
| <font color="blue">**CELL METADATA**</blue> | - | - | - |
| `cell_id` | int | cell ID (combined across all planes, usually already filtered with suite2p `iscell`) | all datasets |  
| `is_redcell` | bool | whether this cell is tagged and indicates projects to `proj_area` | all datasets | 
| `projection` | str | summarize `is_redcell` and `proj_area` for a given cell | all datasets | 
| <font color="blue">**MULTI-SESSION (MUSE) CELL METADATA**</blue> | - | - | - |
| `muse_cell_id` | int | cell index that is global to the animal, across all planes and relevant days, from multi-session (MUSE) registration and manual refinement<br>note: this was renamed from `subject_global_roi_muse` variable in NWB file for easier filtering later | GNG datasets |  
| `muse_cell_persist` | int | number of sessions that the corresponding `muse_cell_id` persisted for<br>note: cells that were tracked in multiple days would have `muse_cell_id > 1` | GNG datasets |
| <font color="blue">**FLUORESCENCE DATA**</blue> | - | - | - |
| `dFF0` | list / array | array of normalized fluorescence of a given cell of a given trial | all datasets |  
| `t` | list / array | array of time (seconds) to accompany `dFF0` vector, relative to start of trial | all datasets |  

The following table describes the multi-session (MUSE) metadata and criteria for up-labeling of omni-present red cells (ORC):

| Name | Type | Description |
| --: | :--: | :-- |
| `subject_id` | int | subject ID |
| `day_num` | int | day / session number of the task (day 0 ~ passive, day > 0 are GNG sessions) |
| `max_sessions` | int | max number of sessions (incl. passive session in GNG dataset) for the animal |
| `cell_id` | int | cell ID, unique to the animal in the session, but **should not** be used as a linkage across sessions |
| `is_redcell` | bool | **original** tag to indicates whether it is a labeled cell |
| `projection` | str | **original** projection area of the labeled cell (`OB`, `mPFC` or `unknown` |
| `muse_cell_id` | int | global cell ID (across sessions), which can be used to link across sessions |
| `muse_cell_persist` | int | number of sessions the cell was tracked across the GNG experiment |
| `muse_cell_sessions` | list of int | list of session / day number, e.g. `[0, 2, 3]` means it was tracked for 3 sessions: passive, day 2, day 3 |
| `muse_cell_sessions_str` | str | essentially `muse_cell_sessions` joined by `\|`, e.g. `0\|2\|3`, to be used for quick lookup if needed |
| `is_redcell_muse` | list of `{0,1}` | list of "boolean" (i.e. `0` ~ False or `1` ~ True) to indicate whether it was a red cell for the sessions in `muse_cell_sessions` list<br>e.g. `muse_cell_sessions=[0, 2, 3]` & `is_redcell_muse=[1, 0, 1]` : labeled as a red cell for only passive session and day 3 GNG |
| `muse_session_redcell_str` | str | essentially combining `muse_cell_persist` and `is_redcell_muse`, with this format: `<session:is_red>`<br>e.g. with the above examples, it would be codified as `0:1\|2:0\|3:1` |
| `is_omni_cell` | bool | whether the cell is considered to be "omni-present", i.e. persisted across all relevant sessions<br>note: we only care about the first 4 sessions (passive day 0, GNG day 1, GNG day 2, GNG day 3) so we only consider it to be "omni" if present for those 4 sessions |
| `is_redcell__<ORC_CRITERA>` | bool | re-labeled (i.e. uplabeled) for whether the cell is considered to be red based on `ORC_CRITERA` (see below) |
| `is_omni_redcell__<ORC_CRITERA>` | bool | whether it is Omni Red Cell (ORC) based on `ORC_CRITERA` (see below)<br>i.e. *AND* combination between `is_omni_cell` and `is_redcell__<ORC_CRITERA>` |
| `projection__<ORC_CRITERA>` | str | re-labeled projection based on `proj_area` if `is_redcell__<ORC_CRITERA> = True`, else turn to `unknown` |
| `projection_omni__<ORC_CRITERA>` | str | re-labeled projection based on `proj_area` if `is_omni_redcell__<ORC_CRITERA> = True`, else turn to `unknown` |

There are 4 differnt criteria for up0labeling (`ORC_CRITERA`), As in, a cell is considered to always be red:

| Criteria | Alias | Description | Note |
| --: | :-- | :-- | :-- |
| `min_1_session` | *min 1* | ... if it is red for at least **1** session | most relaxed |
| `min_3_sessions` | *min 3* | ... if it is red for at least **3** sessions | what we typically use |
| `first_4_sessions` | *first 4* | ... if it is red for the **first 4** sessions (i.e. passive + days 1-3) | strictest for GNG-2021 <br>but not *yet* for GNG-2023b |
| `all_sessions` | *all* | ... if it is red for the **all** sessions <br>(i.e. must match the `max_sessions` for that animal) | strictest for all GNG datasets |

