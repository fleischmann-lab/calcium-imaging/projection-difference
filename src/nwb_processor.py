import os
import re
import time
import warnings

from functools import partial

import numpy as np
import pandas as pd

from scipy import ndimage
from sklearn.preprocessing import StandardScaler

from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

from pynwb import NWBHDF5IO


def construct_muse_metadata(df):
    """
    Construct mult-session (MUSE) cell metadata with different up-labeling criteria
    for omni-present red cell (ORC)

    Parameters
    ----------
    df: data frame containing GNG metadata and fluoresence data, generated from `load_experiment`

    Returns
    -------
    cdf: cell metadata containing multi-session and different up-labeling criteria for ORC
    
    """
    cell_meta_cols = [
        'experiment_name', 'subject_id', 'proj_area', 
        'exp_id', 'date_id', 'day_num',
        'cell_id', 'is_redcell', 'projection',
        'muse_cell_id', 'muse_cell_persist'
    ]

    assert all([
        'GNG' in x.upper() for x in df['experiment_name'].unique()
    ]), 'Only GNG experiments are allowed, the `df` passed in contains non-GNG datasets'
    
    # double-check the persistence column
    __check_muse_persistence__(df)
    
    # cell metadata
    cdf = (
        df.filter(cell_meta_cols)
        .drop_duplicates()
        .reset_index(drop=True)
    )
    
    # codify multi-session list
    cdf = __codify_multi_session_list__(cdf)

    # construct different criteria of for omni-present redcells (ORC)
    cdf = __determine_omni_criteria__(cdf)

    return cdf
    
def __check_muse_persistence__(df):
    """
    Check the `muse_cell_persist` column to make sure things are processed / saved correctly
    See `construct_muse_metadata`
    """
    # original persistence
    orig_persist = (
        df.filter(['subject_id', 'muse_cell_id', 'muse_cell_persist'])
        .drop_duplicates()
        .sort_values(['subject_id', 'muse_cell_id'])
        .reset_index(drop=True)
    )
    # repcomputed persistence
    recomputed_persist = (
        df.drop_duplicates(['date_id', 'subject_id', 'muse_cell_id'])
        .groupby(['subject_id', 'muse_cell_id'])
        ['date_id'].count()
        .to_frame('muse_cell_persist')
        .reset_index()
        .sort_values(['subject_id', 'muse_cell_id'])
        .reset_index(drop=True)
    )

    err_msg = (
        'Mismatch between pre-existing `muse_cell_persist` column and '
        're-computed # persisted sessions for each cell.'
    )
    
    assert orig_persist.eq(recomputed_persist).all().all(), err_msg


def __codify_multi_session_list__(cdf):
    """
    Convert list of session to codify MUSE persistence of a cell
    See `construct_muse_metadata`    
    """

    # create list of multi-sessions
    cdf = cdf.merge(
        cdf.sort_values('day_num')
        .astype({'day_num': 'int', 'is_redcell': 'int'})
        .groupby(['subject_id', 'muse_cell_id'])
        [['day_num', 'is_redcell']]
        .agg(list)
        .rename(columns={
            'day_num': 'muse_cell_sessions',
            'is_redcell': 'is_redcell_muse'
        })
        .reset_index()
    )

    # create str for easier query / look
    # format e.g.: "0:<is_redcell_day_0>|1:<is_redcell_day_1>"
    cdf['muse_session_redcell_str'] = cdf.apply(
        lambda x: '|'.join([
            f'{s}:{r}' for s, r in zip(
                x['muse_cell_sessions'],
                x['is_redcell_muse']
            )
        ]),
        axis=1
    )
    
    cdf['muse_cell_sessions_str'] = (
        cdf['muse_cell_sessions']
        .apply(lambda x: '|'.join(map(str,x)))
    )

    # max sessions per animal
    cdf = cdf.merge(
        cdf.groupby(['subject_id'])
        ['date_id'].nunique()
        .to_frame('max_sessions')
        .reset_index()
    )

    return cdf

def __determine_omni_criteria__(cdf):
    """
    Construct different criteria for omni-present red cells (ORC)
    This is very specific to the GNG dataset in this experiment
    See `construct_muse_metadata`
    """
    # omni here only means exists for the first 4 sessions, not for all sessions
    # because some animals only have 4 days, some 5 days and we're only interested
    # in the aligned sessions [passive, gng 1, gng 2, gng 3]
    
    cdf['is_omni_cell'] = cdf['muse_cell_sessions_str'].str.startswith('0|1|2|3')
    
    cdf['is_redcell__min_1_session'] = cdf['is_redcell_muse'].apply(sum) >= 1
    cdf['is_redcell__min_3_sessions'] = cdf['is_redcell_muse'].apply(sum) >= 3
    
    # note: all here includes day 5 if exists
    cdf['is_redcell__first_4_sessions'] = cdf['is_redcell_muse'].apply(lambda x: sum(x[:4]) == 4)
    cdf['is_redcell__all_sessions'] = cdf['is_redcell_muse'].apply(sum) == cdf['max_sessions']

    # re-assign `projection` column based on different criteria
    for rc_col in cdf.filter(regex='is_redcell__').columns:
        rc_cond = rc_col.split('__')[1]
        orc_col = 'is_omni_redcell__' + rc_cond
        cdf[orc_col] = cdf[rc_col] & cdf['is_omni_cell']
        
        cdf['projection__' + rc_cond] = cdf.apply(
            lambda x: x['proj_area'] if x[rc_col] else 'unknown',
            axis=1
        )
        
        cdf['projection_omni__' + rc_cond] = cdf.apply(
            lambda x: x['proj_area'] if x[orc_col] else 'unknown',
            axis=1
        )

    return cdf


def summarize_task_outcome(nwb):
    """
    Summarize task outcomes from NWB file object
    
    Parameters
    ----------
    nwb: NWB object
    
    Returns
    -------
    A dict that contains GNG task outcomes 
    
    """
    trial_df = nwb.trials.to_dataframe()[['trial_type','outcome']]
    num_trials = len(trial_df)
    
    outcome = dict(
        GO = 0,
        NOGO = 0,
        hit = 0,
        miss = 0,
        correct_rejection = 0,
        false_alarm = 0
    )
    
    outcome.update(trial_df.value_counts('trial_type').to_dict())
    outcome.update(trial_df.value_counts('outcome').to_dict())
    outcome = {
        f'percent_{k.lower()}_trial': 100 * v / num_trials
        for k, v in outcome.items()
    }
    
    outcome['task_accuracy'] = \
        outcome['percent_hit_trial'] + \
        outcome['percent_correct_rejection_trial']
    
    outcome['task_TPR'] = \
        outcome['percent_hit_trial'] / \
        (outcome['percent_hit_trial'] + outcome['percent_miss_trial'])
    
    outcome['task_TNR'] = \
        outcome['percent_correct_rejection_trial'] / \
        (outcome['percent_correct_rejection_trial'] + outcome['percent_false_alarm_trial'])
    
    outcome['task_balanced_accuracy'] = 100 * (
        outcome['task_TPR'] + \
        outcome['task_TNR'] \
    ) / 2

    return outcome

def gather_metadata(file_path):
    """
    Get metadata for NWB file to construct database
    
    Parameters
    ----------
    file_path: path to an NWB file

    Returns
    -------
    A dict that contains experiment session, task, recording metadata 

    Notes
    -----
    Use this for constructing database of metadata across all datasets
    Use `get_metadata` in conjunction with `load_nwb` to process NWB file
    """
    
    with NWBHDF5IO(file_path, load_namespaces=True) as nwb_io:
        nwb = nwb_io.read()
            
        exp_meta = nwb.lab_meta_data['lab_metadata']
        planes = list(nwb.processing['ophys']['Fluorescence'].roi_response_series.keys())
        ophys_shape = np.array([nwb.processing['ophys']['Fluorescence'][p].data.shape for p in planes])
    
        total_frames = np.unique(ophys_shape[:,0])
        assert len(total_frames)
        total_frames = total_frames[0]
    
        total_cells = ophys_shape[:,1].sum()
        total_iscells = nwb.processing['ophys']['ImageSegmentation']['PlaneSegmentation']['iscell'].data[:,0].sum().astype(int)
        
        num_trials = len(nwb.trials)
        num_bad_trials = sum(nwb.trials.is_bad_trial.data[:])
        frames_per_trial = int(total_frames / num_trials)
        
        outcome = dict()
        if exp_meta.experiment_task != "passive":
            outcome = summarize_task_outcome(nwb)
            
        metadata = dict(
            file_path = file_path,
            date_id = nwb.session_start_time.strftime('%Y%m%d'),
            date = nwb.session_start_time,
            subject_id = nwb.subject.subject_id,
            experiment_name = exp_meta.experiment_name,
            experiment_task = exp_meta.experiment_task,
            projection_area = exp_meta.projection_area,
            recording_area = exp_meta.recording_area,
            num_stims = len(nwb.acquisition['odor_table']['stim_id']),
            num_trials = num_trials,
            num_planes = len(planes),
            total_frames = total_frames,
            frames_per_trial = frames_per_trial,
            total_cells = total_cells,
            total_is_cells = total_iscells,
            total_is_redcell = len(nwb.processing['cell_tag']['is_redcell']),
            session_description = nwb.session_description, 
            experiment_description = nwb.experiment_description,
            num_bad_trials = num_bad_trials,
            **outcome
        )
        
    return metadata
    
def get_metadata(nwb, file):
    """
    Get metadata for NWB file to process data
    
    Parameters
    ----------
    nwb: NWB object
    file: file name to check for consistency

    Returns
    -------
    A dict that contains experiment session metadata

    Notes
    -----
    Use this in conjunction with `load_nwb` to process NWB file
    Use `gather_metadata` for constructing database of metadata across all datasets
    """
    subject_id = int(nwb.subject.subject_id)
    file_base = os.path.basename(file)
    if 'sub' in file_base and 'ses' in file_base:
        _subj_id_from_file = re.search('sub-(\d+)_ses', file_base).groups()
    else:
        _subj_id_from_file = re.search('.+_(\d+)_.+', file_base).groups()
    assert len(_subj_id_from_file) == 1, 'missing subject id inside file name'
    _subj_id_from_file = int(_subj_id_from_file[0])
    
    if subject_id != _subj_id_from_file: 
        warnings.warn(
            f'The NWB `subject_id` ({subject_id}) does not match the one in the file ({_subj_id_from_file}). '\
            'Using the one from file'
        )
        subject_id = _subj_id_from_file
        
    proj_area = nwb.lab_meta_data['lab_metadata'].projection_area
    experiment_task = nwb.lab_meta_data['lab_metadata'].experiment_task
    experiment_name = nwb.lab_meta_data['lab_metadata'].experiment_name
    recording_area = nwb.lab_meta_data['lab_metadata'].recording_area
    date_id = nwb.session_start_time.strftime('%Y%m%d')
    num_redcells = len(nwb.processing['cell_tag']['is_redcell'])
    
    metadata = dict(
        subject_id = subject_id,
        proj_area = proj_area,
        date_id = date_id,
        num_redcells = num_redcells,
        experiment_task = experiment_task,
        experiment_name = experiment_name,
        recording_area = recording_area,
    )
    return metadata


def load_nwb(file, compute_dFF0_kwargs=dict(), extra_trial_columns = [], verbose=True):
    """
    Load NWB file
    
    Parameters
    ----------
    file: file path for NWB file
    compute_dFF0_kwargs: configuration dict for computing dFF0
    verbose: verbosity during processing
    
    Returns
    -------
    A dict that contains metadata, stimuli data and dFF0 activity
    
    """
    with NWBHDF5IO(file, 'r', load_namespaces=True) as nwb_io:
        nwb = nwb_io.read()
        metadata = get_metadata(nwb, file)
        exp_task = metadata.get('experiment_task', 'passive')
        if exp_task == "passive":
            stimulus_data = extract_odor(
                nwb, stim_source='stimulus', stim_key='odor',
                remap=True, verbose=verbose
            )
        else:
            assert exp_task in ["go_nogo", "go-nogo", "active"]
            stimulus_data = extract_odor(
                nwb, stim_source='trials', stim_key='odor_raw_id', 
                remap=True, verbose=verbose
            )
            
        neural_activity = compute_dFF0(
            nwb, 
            use_redcells=True, 
            verbose=verbose,
            **compute_dFF0_kwargs
        )

        neural_activity['dFF0'] = np.stack(neural_activity['dFF0']).transpose((2,0,1))
        neural_activity['t'] = np.stack(neural_activity['t'])
        assert neural_activity['dFF0'].shape[1:] == neural_activity['t'].shape

        ncell_dF = neural_activity['dFF0'].shape[0] 
        if 'is_redcell' in neural_activity:
            assert ncell_dF == len(neural_activity['is_redcell'])
            assert neural_activity['is_redcell'].sum() == metadata['num_redcells']
        else:
            assert ncell_dF == metadata['num_redcells']

        is_bad_trial = list(nwb.trials.is_bad_trial[:])
        stimulus_data['stim_onset_frame'] = list(nwb.trials.stim_onset_frame[:])
        stimulus_data['stim_offset_frame'] = list(nwb.trials.stim_offset_frame[:])

        extra_trial_data = dict()
        trial_df = nwb.trials.to_dataframe()
        num_trials = len(trial_df)
        for k in extra_trial_columns:
            if k in stimulus_data or k == 'is_bad_trial':
                continue
            if k in nwb.trials:                
                extra_trial_data[k] = list(trial_df[k])
            else:
                extra_trial_data[k] = [None] * num_trials
                
        output = dict(
            **metadata,
            **stimulus_data,
            **neural_activity,
            cell_id = range(ncell_dF),
            trial_id = range(len(is_bad_trial)),
            is_bad_trial = is_bad_trial,
            **extra_trial_data
        )
    
    return output


def __load_one_nwb_file__(file, load_kwargs=dict(), ignore_warnings=False):
    """
    Load 1 NWB file, function to faciliate parallel loading
    """
    _dat = None
    with warnings.catch_warnings():
        if ignore_warnings:
            warnings.filterwarnings('ignore')
        try:
            _dat = load_nwb(file, **load_kwargs)
        except Exception as e:
            print(f'ERROR at file "{file}", skipping.\n{e}')
    return _dat


def load_experiment(
    nwb_files = None,
    database_filepath = None,
    experiment_name = None,
    compute_dFF0_kwargs = dict(),
    verbose = False,
    ignore_warnings = False,
    use_parallel = True,
    max_workers = -1,
    unknown_proj_label = 'unknown',
    to_df = True,
    extra_trial_columns = [],
    drop_misc_stim_cols = True
):
    """
    Load multiple NWB files in an experiment and returns a dataframe
    
    Parameters
    ----------
    nwb_files: list of NWB files
    database_filepath: database file path to read from
    experiment_name: experiment name in the database file, can also be a list
    compute_dFF0_kwargs: compute dFF0 config
    verbose: whether to be verbose
    ignore_warnings: whether to print out warnings during loading
    use_parallel: whether to load files in parallel
    max_workers: maximum number of workers
    to_df: combine to a dataframe
    unknown_proj_label: what to use for non redcell projection area in dataframe   
    extra_trial_columns: extra trial columns to save if available
    drop_misc_stim_cols: whether to drop misc stim columns, `stim_types` and `stim_annotation` 
        only applicable for `to_df=True`.
    """
    misc_stim_cols = ['stim_types', 'stim_annotation']
    
    assert experiment_name is not None, 'Cannot have empty `experiment_name`'
    assert (nwb_files is not None) + (database_filepath is not None) == 1,\
        'Between `nwb_files` and `database_filepath`, only 1 can be empty'

    if isinstance(experiment_name, str):
        experiment_name = [experiment_name]
        
    if nwb_files is None:        
        db_meta = pd.read_csv(database_filepath, comment='#')
        select_db = db_meta.query('experiment_name in @experiment_name')
        nwb_files = select_db['file_path'].to_list()

    assert all(list(map(os.path.exists, nwb_files))), \
        'At least one of the files in the `nwb_files` list does not exist:\n' \
        f'{nwb_files}'            
    
    if to_df:
        allowed_exps = [
            "identity_dataset", 
            "concentration_dataset",
            # "fast_hz_sniff_trig_dataset", # TBD
            "GNG_2021_dataset",
            "GNG_2023b_dataset",            
        ]
        assert all([x in allowed_exps for x in experiment_name]), \
            f'To use `to_df = True`, only the following `experiment_name` is allowed:\n{allowed_exps}'
    
    print(f'The following NWB files are going to be processed for experiment(s)="{experiment_name}"\n:{nwb_files}')

    l1f_kwargs = dict(
        load_kwargs = dict(
            compute_dFF0_kwargs = compute_dFF0_kwargs,
            verbose = verbose,
            extra_trial_columns = extra_trial_columns
        ),
        ignore_warnings = ignore_warnings
    )
    
    if use_parallel:
        max_cpus = len(os.sched_getaffinity(0))
        if max_workers < 1:
            max_workers = max_cpus
            
        max_workers = min(max_workers, len(nwb_files))
        data = process_map(
            partial(__load_one_nwb_file__, **l1f_kwargs),
            nwb_files,
            max_workers=max_workers,
            desc = 'NWB files'
        )
    else:
        data = []
        for file in tqdm(nwb_files, desc = 'NWB files'):
            data.append(__load_one_nwb_file__(
                file = file,
                **l1f_kwargs,
            ))
         
    data_exps = [d['experiment_name'] for d in data]
    lowercase_expnames = [x.lower() for x in experiment_name]
    assert all([x.lower() in lowercase_expnames for x in data_exps]),\
        f'There seems to be a mismatch between experiment names from data:\n{data_exps}'

    print('\t DONE with loading NWB files.')
    
    if not to_df:
        return data    
    
    print('Continuing to convert to a dataframe')

    # each row will a cell in a trial
    known_cell_dim_cols = ['dFF0', 'is_redcell']
    kwown_trial_dim_cols = ['dFF0', 't']
    
    cols_to_drop = ['info', 'stim_start', 'stim_stop']
    
    df = pd.DataFrame(data).drop(columns=cols_to_drop, errors='ignore')
    
    # construct column categories to explode
    # columns with cell dimensions
    cols_with_cell_dim = list(set(
        known_cell_dim_cols + \
        [
            x for x in df.columns 
            if (
                x.startswith('cell_') or
                x.endswith('_cell') or
                x.startswith('muse_cell_')
            )
        ]
    ))
    
    cols_with_cell_dim = [
        x for x in cols_with_cell_dim 
        if x in df.columns
    ]
    
    # columns with trial dimensions
    cols_with_trial_dim = list(set(
        kwown_trial_dim_cols + \
        [
            x for x in df.columns
            if (
                x.startswith('trial_') or \
                x.endswith('_trial') or \
                x.startswith('stim_') or \
                x.endswith('_stim')
            )
        ] + \
        extra_trial_columns
    ))
    
    cols_with_trial_dim = [
        x for x in cols_with_trial_dim
        if x in df.columns
    ]
    
    # explode -> each row will a cell in a trial
    df = (
        df
        .explode(cols_with_cell_dim)
        .explode(cols_with_trial_dim)
        .reset_index(drop=True)
        .astype({'is_bad_trial': 'bool', 'is_redcell': 'bool'})
    )
    
    # additional processing of metadata for conc / GNG datasets
    conc_in_exp = any(['concentration' in x.lower() for x in experiment_name])
    gng_in_exp = any(['gng' in x.lower() for x in experiment_name])
    
    assert not (conc_in_exp and gng_in_exp), \
        'Currently do not support processing both concentration and GNG experiment in the same data frame'
    
    df['stim_type'] = df['stim_types'].apply(lambda x: x[0]) # assign the first one
    
    if conc_in_exp:
        df = process_concentration_metadata(df)
        
    if gng_in_exp:
        df = process_GNG_metadata(df)
        
    if drop_misc_stim_cols:
        df = df.drop(columns=misc_stim_cols, errors='ignore')

    # assign process projection
    df['projection'] = df.apply(
        lambda x: x['proj_area'] if x['is_redcell'] 
            else unknown_proj_label,
        axis=1
    )

    # experiment session ID, by combing columns
    df['exp_id'] = (
        df['proj_area'] + 
        '_' + 
        df['subject_id'].astype(str) + 
        '_' +
        df['date_id'].astype(str)
    )
    
    # re-order columns (push timeseries columns to end)
    tseries_columns = ['t'] + list(df.filter(regex='.*dFF0').columns)
    metadata_columns = [x for x in df.columns if x not in tseries_columns]
    df = df[metadata_columns + tseries_columns]
    
    print('\t DONE')
    
    return df


def process_concentration_metadata(df):
    # in concentration dataset:
    # stim_types = [stim_type, stim_class, stim_conc]
    # stim_type: odor, control
    # stim_class: A, B, C (i.e. stim identity)
    # stim_conc: low, medium, high
    
    df['stim_type'] = df['stim_types'].apply(
        lambda x: x[0]
    )
    
    df['stim_class'] = df['stim_types'].apply(
        lambda x: x[1] if len(x) == 3 else x[0]
    )
    
    df['stim_conc'] = df['stim_types'].apply(
        lambda x: x[-1] if len(x) == 3 else x[0]
    )

    return df


def process_GNG_metadata(df):
    # in GNG dataset:
    # stim_types = [stim_type, stim_name_value, stim_task_tag]
    # stim_type: odor, control
    # stim_name_value: CS+/-1, CS+/-2, US1/2
    # stim_task_tag: pre-task, during-task

    df['stim_name'] = df['stim_name'].str.lower()
    
    df['stim_type'] = df['stim_types'].apply(
        lambda x: x[0]
    )

    df['stim_name_value'] = df['stim_types'].apply(
        lambda x: x[1] if len(x) > 1 else x[0]
    )
    df['stim_task_tag'] = df['stim_types'].apply(
        lambda x: x[-1] if len(x) == 3 else x[0]
    )
    
    # assign day number of task days (day 0 ~ passive, day > 0 ~ task / GNG)
    df['day_num'] = (
        df
        .groupby('experiment_name')
        ['date_id'].rank(method='dense')
        .astype('int')
    ) - 1
    
    # stimulus valence metadata
    df['stim_id'] = df['stim_id'].astype('int')
    
    df['stim_valence'] = df['stim_name_value'].map({
        'CS-1': 'none',
        'CS-2': 'none',
        'CS+1': 'reward',
        'CS+2': 'reward',
    }).fillna('control')
    
    df['stim_valence_id'] = df['stim_valence'].map({
        'control': 0,
        'none': 1,
        'reward': 2
    })
    
    # outcome metadata
    if 'outcome' in df.columns:
        df['outcome'] = df['outcome'].fillna('none')
        df['outcome_id'] = df['outcome'].map({
            'none':0,
            'hit':1,
            'miss':2,
            'correct_rejection':3,
            'false_alarm':4
        })
        
        df['has_reward'] = df['has_reward'].fillna(False)
    
        # action outcome metadata
        df['action'] = df['outcome'].map({
            'control': 'none',
            'hit': 'correct',
            'correct_rejection': 'correct',
            'false_alarm': 'incorrect',
            'miss': 'incorrect',
        }).fillna('none')
        
        df['action_id'] = df['action'].map({
            'none': 0,
            'incorrect': 1,
            'correct': 2
        })
    
    # trial type / stim type metadata
    if 'trial_type' in df.columns:
        df['trial_type'] = df['trial_type'].fillna('control')
        df['trial_type_id'] = df['trial_type'].map({
            'control': 0,
            'NOGO': 1,
            'GO': 2
        })
        
    # set dtype for consistency
    dtype_dict = {
        'stim_onset_frame': 'int',
        'stim_offset_frame': 'int',

        'muse_cell_id': 'int',
        'muse_cell_persist': 'int',
        'cell_id': 'int',
        'trial_id': 'int',
        'wait_for_lick_onset': 'float',
        'reward_onset': 'float',
        
        'cell_centroid_x': 'float',
        'cell_centroid_y': 'float',
        'cell_centroid_z': 'int',
    }

    dtype_dict = {
        k: v for k, v in dtype_dict.items()
        if k in df.columns
    }
    
    df = df.astype(dtype_dict)

    return df


def split_by_trial(nwb, data, time, axis=0, reshift_time=True):
    """
    Split `data` (in `axis` [default: `0`] axis as `data` time axis) 
    extracted from `nwb` object into trials, using `time` array.
    
    If `reshift_time` [default: `True`], each trial's time vector
    will be offset to begin at 0.
    
    Returns: list of `data_by_trial` and `time_by_trial`. 
    
    If the time points do not match, will attempt to take the minimum (throw warning).
    TODO: throw error instead. This is dangerous.
    """
    data_by_trial, time_by_trial = [], []
    
    if (ntpts_data := data.shape[axis]) != (ntps_time := len(time)):
        accepted_npts = min(ntpts_data, ntps_time)
        warnings.warn(
            f'DANGER DANGER DANGER!\n\t The number of timepoints between `data` (of `axis={axis}`: {ntpts_data})' + \
            f'and `time` ({ntps_time}) do not match.\n\t Will proceed with the minimum of time points ({accepted_npts}).' + \
            f'\n\t DANGER DANGER DANGER!\n\t Please proceed with ABSOLUTE CAUTION!'
        )
        time = time[:accepted_npts]
        data = np.take(data, range(accepted_npts), axis=axis)
        
    for _, row in nwb.trials.to_dataframe().iterrows():
        trial = (time >= row.start_time) & (time <= row.stop_time)
        _data = np.take(data, np.where(trial)[0], axis=axis)
        _time = time[trial]        
        if reshift_time:
            _time -= _time[0]            
        data_by_trial.append(_data)
        time_by_trial.append(_time)
        
    return data_by_trial, time_by_trial 


def throw_potential_warning(obj, message_sources=['description', 'comments'],
                            keywords=['warn', 'danger', 'error', 'caution']):
    """
    Throw potential warnings if one of the sources in `message_sources` of `obj`
    contains of the words in `keywords`
    """
    if len(message_sources) * len(keywords) == 0:
        return
    obj_name = obj.name
    for s in message_sources:
        if not hasattr(obj, s):
            continue
        message = getattr(obj, s)
        if message is None: 
            continue
        if len(message) == 0:
            continue            
        if any([k in (m:=message.lower()) for k in keywords]):
            warnings.warn(
                f'A warning was caught in `{obj_name}.{s}`\n' +\
                message \
                + '\n\tPlease procceed with caution!'
            )


def process_odor_on(odor_on, t_odor, min_k=2, dk_eps=1):
    """
    Return odor stimulus window `stim_start` and `stim_stop` times if any
    `odor_on`: array of whether odor is on
    `t_odor`: corresponding time vector
    `min_k`: minimum number of points needed to be on if any
    `dk_eps`: lenience num points when checking the index diff
    """
    loc = np.squeeze(np.where(odor_on > 0))
    if len(loc) == 0:
        return dict(t_on = None, t_off = None)
    assert len(loc) >= min_k, 'Insufficient "on" points.'
    assert all(np.diff(loc) <= dk_eps), \
        'The difference between consecutive "on" indices excced allowable range'
    return dict(
        stim_start = t_odor[loc[0]], 
        stim_stop = t_odor[loc[-1]]
    )


def cast_column_dtype(df, column, overwrite=True):
    allowed_dtype_map = {
        'int': int,
        'str': str
    }
    allowed_dtypes = list(allowed_dtype_map.keys())
    dtype_col = column + '_dtype'
    if dtype_col not in df.columns:
        return df
    value_vec = df[column].to_numpy()
    dtype_vec = df[dtype_col].str.lower().to_numpy()
    assert all([d in allowed_dtypes for d in list(set(dtype_vec))]),\
        f'Found a non-allowed dtype in the dtype vector of {column}. '\
        f'{set(dtype_vec)}\n'\
        f'Only allowed {allowed_dtypes}'
    caster = [allowed_dtype_map[d] for d in dtype_vec]
    casted_vec = np.array([c(v) for c,v in zip(caster, value_vec)])
    if overwrite:
        df[column] = casted_vec
    else:
        df['__' + column + '__'] = casted_vec
    return df


def extract_odor(
    nwb,
    stim_source='stimulus', # 'stimulus', or 'trials'
    stim_key='odor', # 'odor', or 'odor_raw_id' 
    remap=True,
    odor_table_source='acquisition',
    convert_odor_dtype='int',
    warn=True,
    verbose=True
):
    """
    Extract odor identities from `nwb` object per trial
    `stim_source` and `stim_key`: stimulus source and key.
        `nwb.<stim_source>[<stim_key>]`
    `remap`: whether to use the `odor_table` in `odor_table_source` to remap IDs, default: `True`
    `odor_table_source`: where to get the `odor_table`, default: `'acquisition'`
    Returns: a list of dict of trals of `odor_id`, and extra metadata from odor table or odor_on
    """
    # obtain odor ids per trial
    odor_id = getattr(nwb, stim_source).get(stim_key, None)
    assert odor_id is not None, 'Could not find odor stimuli values with ' \
        f'`nwb.{stim_source}["{stim_key}"]`'
    
    if hasattr(odor_id, 'timestamps'):
        assert stim_source != "trials", f'Data from "{stim_source}" should not come from `trials`'
        odor_id, _ = split_by_trial(nwb, odor_id.data[:], odor_id.timestamps[:])
        odor_id = list(map(np.unique, odor_id))
        assert all([len(x) == 1 for x in odor_id]), 'Some trials have multiple odors'

        odor_id = np.array(odor_id).flatten()
        if convert_odor_dtype is not None:
            odor_id = odor_id.astype(convert_odor_dtype)
    else:
        odor_id = np.array(odor_id.data[:])
        
    additional_outputs = dict()
    
    # remapping based on odor table     
    if remap:
        odor_table = getattr(nwb, 'acquisition').get('odor_table', None)     
        assert odor_table is not None, \
            f'`odor_table` is not found in {odor_table_source}. ' +\
            'Either change `remap=False` or `odor_table_source` to the proper source.'
        throw_potential_warning(odor_table)
        
        odor_df = odor_table.to_dataframe()
        odor_df = cast_column_dtype(odor_df, 'raw_id')
        odor_df = cast_column_dtype(odor_df, 'stim_id')

        odor_dict = odor_df.set_index('raw_id').stim_id.to_dict()
        odor_raw_id_acq = odor_id.copy()
        
        odor_id = pd.Series(odor_raw_id_acq).map(odor_dict).to_numpy()        
        
        extra_stim_cols = {
            'stim_type': 'stim_types',
            'stim_types': 'stim_types',
            'stim_name': 'stim_name'
        }
        assert not all([x in odor_df.columns for x in ['stim_type', 'stim_types']])
        for k, new_k in extra_stim_cols.items():
            if k not in odor_df.columns: 
                continue
            extra_data_map = odor_df.set_index('raw_id')[k].to_dict()
            odor_extra = pd.Series(odor_raw_id_acq).map(extra_data_map)
            if type(list(extra_data_map.values())[0]) in [list, tuple, set, np.ndarray]:
                odor_extra = odor_extra.apply(list)
            additional_outputs.update({new_k: odor_extra.to_list()})
            
    # stimulus onset and offset per trial
    stim_times = dict()
    if 'odor_on' in nwb.stimulus:
        odor_on = nwb.stimulus['odor_on']
        odor_on, t_odor = split_by_trial(nwb, odor_on.data[:], odor_on.timestamps[:])
        odor_times = pd.DataFrame([process_odor_on(_odor, _t) 
                                   for _odor, _t in zip(odor_on, t_odor)])
        stim_times = dict(
            stim_start = odor_times.stim_start.to_numpy(),
            stim_stop = odor_times.stim_stop.to_numpy()
        )
    
    if 'odor_table' in nwb.acquisition: 
        odor_table = nwb.acquisition['odor_table'].to_dataframe()
        stim_time_keys = ['stim_onset_frame', 'stim_offset_frame']
        for k in stim_time_keys:
            if k not in odor_table:
                continue 
                
            v = odor_table[k]
            assert k not in stim_times, f'the key `{k}` is already in `stim_times`'
            assert v.nunique() == 1, f'heterogeneous `odor_table["{k}"]` is not allowed right now'
            stim_times[k] = v[0]

            if warn:
                warnings.warn(f'The integration of the key `{k}` from `odor_table` is not generalizable for now. Careful!')
    
    additional_outputs.update(stim_times)
    
    # combine
    output = dict(
        stim_id = odor_id,
        **additional_outputs
    )
    
    return output
    
    
def norm_F_matrix_running_percentile(F, Fs, tau_m=None, tau_p=100, p=10, norm_type='dFF0'):
    """
    Normalize fluorescence matrix `F` sampled at `Fs` Hz by using percentile filter
    
    - `F`: array of size `time x rois` of fluorescence (or neuropil-corrected fluorescence)
    - `Fs`: sampling rate of `F` (unit: Hz) for the time (i.e. first) dimension of `F`
    - `tau_m` [default: `None`]: amount in seconds for median filter first, to remove outlier noise.
        If this is `None`, no median filter is performed. If this is used, recommend to keep it small (< 2 sec).
        To avoid losing calcium transient amplitudes, keep this as `None`.
    - `tau_p` [default: `100`]: amount in seconds for percentile filter, to compute `F0`.
        This cannot be empty or `None`. Use large values, e.g. at least worth a few trials.
    - `p` [default: `10`]: percentile to apply for percentile filter. Literature ususually use between 5-15.
    - `norm_type` [default: `None`]: how to normalize when having `F` and `F0`. 
        "dFF0": (F - F0) / F0
        "zdFF0": zscore[(F - F0) / F0]
        "dFF": (F - F0) / std(F - F0)
        
    TODO: need to account for when `F0 = 0`, e.g. with:
        F0 = np.maximum(
            ndimage.percentile_filter(F, p, (k_p, 1)),
            1.4826 * ndimage.median_filter(np.abs(F - ndimage.median_filter(F, (k_p,1))), (k_p, 1))
        )
    """
    # convert filter window from seconds to frames (make sure odd number)
    allowed_norm_types = ["dFF0", "zdFF0", "dFF"]
    
    k_m = None
    if tau_m:
        k_m = 2 * round(Fs * tau_m / 2) + 1
        
    assert tau_p is not None, '`tau_p` cannot be `None`'
    assert norm_type in allowed_norm_types, \
        f'`norm_type` needs to be in {allowed_norm_types}, not {norm_type}'
    
    k_p = 2 * round(Fs * tau_p / 2) + 1
    
    # median filter to remove outlier noise
    if k_m:
        F = ndimage.median_filter(F, (k_m, 1))
        
    # percentile filter to calc running baseline
    F0 = ndimage.percentile_filter(F, p, (k_p, 1))
    
    # normalize
    if norm_type == "dFF0":
        dFF0 = (F - F0) / F0
    elif norm_type == "zdFF0":
        dFF0 = (F - F0) / F0
        mu = dFF0.mean(axis=0, keepdims=True)
        sigma = dFF0.std(axis=0, keepdims=True)
        dFF0 = (dFF0 - mu) / sigma
    elif norm_type == "dFF":
        dFF0  = F - F0
        sigma = dFF0.std(axis=0, keepdims=True)
        dFF0 = dFF0 / sigma
        
    return dFF0
    

def compute_dFF0(
    nwb, 
    k_neu=0.7, 
    combine_planes=True, 
    use_iscell=True, 
    use_redcells=False,
    keep_only_redcells=True,
    return_is_redcell=False,
    split_trial=True, 
    return_only_raw=False,
    norm_F_standard=True,
    norm_F_percentile=False, 
    norm_kwargs=dict(),
    return_centroids=False,
    return_muse=False,
    verbose=True
): 
    """
    Compute dF/F0 from fluorescence data inside `nwb`, separated by trials if `split_trial`
    TODO: use string argument instead to choose between `norm_F_standard` and `norm_F_matrix_running_percentile`
    
    - `k_neu`: [default: `0.7`] scalar to subtract neutropil, i.e. `F = F - k_neu x F_neu`
    - `combine_planes`: [default: `True`] whether to combine all the planes in one dataset.
        Warning: the timings of different planes are not aligned, and 
        and there is no alignment/reshifting/resampling happening here.
        If `False`, there will be a field in the sub-dict to indicate each field
    - `use_iscell`: [default: `True`] whether to use `iscell` from `suite2p` output
    - `use_redcells`: [default: `False`] whether to use redcell table
    - `keep_only_redcells`: [default: `True`] whether to keep only redcells.
        This only applies when `use_redcells=True`.
        If `keep_only_redcells=False, use_redcells=True`, it is recommended to have `return_is_redcell=True`
    - `return_is_redcell`: [default: `False`] whether to output redcell ID vector as well
        This only applies when `use_redcells=True`.
    - `split_trial`: [default: `True`] whether to split by trial, recommended 
        as normalization done on a trial basis is better 
        (for baseline estimation during normalization).
        Note: currently to split trials, the timestamps of the first plane is used,
        see <https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/92>
    - `return_only_raw`: [default: `False`] whether to return only the raw `F` and `Fneu`.
        If this is `True`, all parameters about normalization (`norm*`) will be IGNORED
        and will NOT return `dFF0` in the output dict.
    - `norm_F_standard`: [default: `True`] standardize `F`, if `True` will ignore `norm_kwargs`. 
        However, cannot have both `norm_F_standard = norm_F_matrix_running_percentile = True`.
    - `norm_F_percentile`: [default: `False`] whether to normalize using `norm_F_matrix_running_percentile` function with 
        arguments from a dict of `norm_kwargs` (see `norm_F_matrix_running_percentile` for more info, except for `Fs`).
    - `return_centroids`: [default: `False`] return the centroids of each ROI,
        with mean `x` and `y` from segmentation table; and `plane` or `z`.
    - `return_muse`: [default: `False`] return the multisession registration results if prewsent
    
    Returns: a dict of `info` (the params), `t` (time) and `dFF0` (or `F` and `Fneu`) data of
    - (not `split_trial`, not `combine_planes`): each sub-dict of planes contains `all_times x rois_per_plane`
    - (`split_trial`, not `combine_planes`): each sub-dict of planes contains `list[times_per_trial x rois_per_plane]` (`len = num_trials`)
    - (not `split_trial`, `combine_planes`): array of `all_times x all_rois`, by concatenating ROIs across planes
    - (`split_trial`, `combine_planes`): `list[times_per_trial x all_rois]` (`len = num_trials`)
    
    
    NOTE: The red cells processing here very heavily depends on certain assumptions of SD data
        and this function would not generalize well for experiments with red cell tagging 
        when using `use_redcells=True`
        
    """
    if return_only_raw:
        norm_F_standard = False
        norm_F_percentile = False
        norm_kwargs = dict()
        
    assert not (norm_F_standard and norm_F_percentile), \
        'Cannot have both `norm_F_standard = norm_F_percentile = True`. ' \
        'Recommend to use only `norm_F_standard=True` for now'
    
    assert any([return_only_raw, norm_F_standard, norm_F_percentile]), \
        'Need at least one of these to be `True`: '\
        '`return_only_raw`, `norm_F_standard`, `norm_F_percentile`'
    
    F = nwb.processing['ophys']['Fluorescence']
    Fneu = nwb.processing['ophys']['Neuropil']
    deconv = nwb.processing['ophys']['Deconvolved']
    
    segm_obj = nwb.processing['ophys']['ImageSegmentation']['PlaneSegmentation']
    segm_df = segm_obj.to_dataframe()
    
    planes = sorted(F.roi_response_series.keys())
    
    if use_iscell: 
        iscell = segm_obj['iscell'].data[:,0] > 0
        nc_roi = 0
    
    if use_redcells:
        assert use_iscell, 'Also need `use_iscell=True` when using `use_redcells=True`'
        red_indices = nwb.processing['cell_tag']['is_redcell'].to_dataframe()
        red_indices['roi_index'] = red_indices['roi_index'].astype('int')
        red_indices = red_indices.groupby('plane').agg(list).roi_index.to_dict()
    
    # compute centroids (regardless of return or not)
    coord_df = None
    if return_centroids:
        assert use_iscell, 'Also need `use_iscell=True` when using `return_centroids=True`'
        coord_df = (
            segm_df['voxel_mask'].apply(
                lambda x: pd.DataFrame(x).mean()
            )
            .drop(columns='weight')
            .add_prefix('cell_centroid_')
            .reset_index(drop=True)
        )
        assert 'cell_centroid_z' in coord_df.columns, \
            'Missing z centroid (i.e. plane) in segmentation table'
        if 'plane' in segm_df.columns:
            assert (
                coord_df['cell_centroid_z'].astype('int')
                .equals(segm_df['plane'].astype('int'))
            ), 'The z centroid of ROI do not seem to match the planes'
    
    # ignore this if not `use_redcells`
    return_is_redcell = return_is_redcell and use_redcells
    
    if return_only_raw:
        output = dict(
            F = dict(),
            Fneu = dict(),
            deconv = dict()
        )
    else:
        output = dict(
            dFF0 = dict()
        )
        
    output.update(dict(
        t = dict(),
        info = dict(
            k_neu = k_neu,
            combine_planes = combine_planes,
            use_iscell = use_iscell,
            split_trial = split_trial,
            return_only_raw = return_only_raw,
            norm_F_standard = norm_F_standard,
            norm_F_percentile = norm_F_percentile,
            norm_kwargs = norm_kwargs,
            return_centroids=return_centroids,
            return_muse=return_muse
        )
    ))
    
    if return_is_redcell:
        output['is_redcell'] = dict()
    
    centroid_keys = ['cell_centroid_x', 'cell_centroid_y', 'cell_centroid_z']
    if return_centroids:
        assert coord_df is not None, 'Empty coordinate frame'
        for k in centroid_keys:
            assert k in coord_df, f'{k} should be in `coord_df`'
            output[k] = dict()
    
        
    # multi-session data
    muse_df = None
    has_muse = False
    muse_keys = {
        'subject_global_roi_muse': 'muse_cell_id',
        'num_sessions_persisted_muse': 'muse_cell_persist'
    }
    has_muse = all([k in segm_df.columns for k in muse_keys.keys()])
    return_muse = return_muse and has_muse
    
    if return_muse:
        muse_df = (
            segm_df
            .filter(list(muse_keys.keys()))
            .rename(columns=muse_keys)
        )
        for k in muse_keys.values():
            output[k] = dict()

    muse_keys = list(muse_keys.values())
    
    # loop through planes
    for i, p in enumerate(planes):
        F_p = F[p].data[:]
        Fn_p = Fneu[p].data[:]
        dc_p = deconv[p].data[:]
        
        t_p = F[p].timestamps[:]
        assert all(t_p - Fneu[p].timestamps[:] == 0)
        assert all(t_p - deconv[p].timestamps[:] == 0)
        Fs = 1/(t_p[1] - t_p[0])
        
        if i == 0: 
            t_p0 = t_p
            
        is_rc_p = []
        
        if use_iscell:
            cell_mask = iscell[nc_roi:nc_roi+F_p.shape[1]] # boolean mask
            
            if return_centroids:
                cent_p = (
                    coord_df
                    .iloc[nc_roi:nc_roi+F_p.shape[1]]
                    .reset_index(drop=True)
                    .to_dict('list')
                )
                cent_p = {k: np.array(v) for k, v in cent_p.items()}
                cent_p['cell_centroid_z'] = cent_p['cell_centroid_z'].astype('int')
                assert all(cent_p['cell_centroid_z'] == i), \
                    f'Mismatch in plane index `{i}` and the following ' \
                    f"`cell_centroid_z` during processing iscell: \n {cent_p['cell_centroid_z']}"
            
            if return_muse:
                muse_p = (
                    muse_df
                    .iloc[nc_roi:nc_roi+F_p.shape[1]]
                    .reset_index(drop=True)
                    .to_dict('list')
                )
                muse_p = {k: np.array(v) for k, v in muse_p.items()}
                
                    
            is_rc_p = np.full(len(cell_mask), fill_value=False)
            
            nc_roi += F_p.shape[1]
            
            if use_redcells:
                red_idx = red_indices.get(i, [])
                
                # ONLY valid for SD data: a plane w/o any red cell
                # tends to have issues, such as duplicated planes
                if len(red_idx) == 0:
                    continue
                    
                assert all(cell_mask[red_idx]), 'Mismatch iscells and redcells'

                is_rc_p[red_idx] = True    
                
                if keep_only_redcells:
                    cell_mask = red_idx # integer index locations

            F_p = F_p[:,cell_mask]
            Fn_p = Fn_p[:,cell_mask]
            dc_p = dc_p[:,cell_mask]
            is_rc_p = is_rc_p[cell_mask]
            
            if return_centroids:
                for c_k in centroid_keys:
                    cent_p[c_k] = cent_p[c_k][cell_mask]
                    assert len(cent_p[c_k]) == F_p.shape[1], \
                        f'Mismatch between {c_k} (len={len(cent_p[c_k])}) ' \
                        f'and F_p shapes ({F_p.shape}) for number of ROIs'  

            if return_is_redcell:
                assert len(is_rc_p) == F_p.shape[1], \
                    f'Mismatch is_redcell (len={len(is_rc_p)}) ' \
                    f'and F_p shapes ({F_p.shape}) for number of ROIs'  
            
            if return_muse:
                for m_k in muse_keys:
                    muse_p[m_k] = muse_p[m_k][cell_mask]
                    assert len(muse_p[m_k]) == F_p.shape[1], \
                        f'Mismatch between {m_k} (len={len(muse_p[m_k])}) ' \
                        f'and F_p shapes ({F_p.shape}) for number of ROIs'  
                    
        activ_p = F_p - k_neu * Fn_p
        
        if norm_F_standard:
            activ_p = StandardScaler().fit_transform(activ_p)
        
        if norm_F_percentile:
            activ_p = norm_F_matrix_running_percentile(activ_p, Fs, **norm_kwargs)
            
        if split_trial:
            # reference time is from first plane, 
            # currently plane_3 last time per trial is tinily > stop_time of that trial
            activ_p, t_p = split_by_trial(nwb, activ_p, t_p0)
            F_p, _ = split_by_trial(nwb, F_p, t_p0)
            Fn_p, _ = split_by_trial(nwb, Fn_p, t_p0)
            dc_p, _ = split_by_trial(nwb, dc_p, t_p0)
        
        if return_only_raw:
            output['F'][p] = F_p       
            output['Fneu'][p] = Fn_p            
            output['deconv'][p] = dc_p            
        else:
            output['dFF0'][p] = activ_p      
            
        output['t'][p] = t_p
        
        if return_is_redcell:
            output['is_redcell'][p] = is_rc_p
        
        if return_centroids:
            for c_k in centroid_keys:
                output[c_k][p] = cent_p[c_k].copy()
                
        if return_muse:
            for m_k in muse_keys:
                output[m_k][p] = muse_p[m_k].copy()
            
    p0 = list(output['t'].keys())[0]
    
    activ_keys = ['F', 'Fneu', 'dFF0', 'deconv']
    if combine_planes:
        # TODO: raise warning
        if not split_trial:
            for k in activ_keys:
                if k not in output:
                    continue
                output[k] = np.hstack(list(output[k].values()))
        else:
            # this might be better handled by xarray
            for k in activ_keys:
                if k not in output:
                    continue  
                num_trials = len(output[k][p0])
                output[k] = [
                    np.hstack(list(
                        map(lambda x: x[trial], 
                            output[k].values()
                           )))
                    for trial in range(num_trials)
                ]
        output['t'] = output['t'][p0]
        
        if return_is_redcell:
            output['is_redcell'] = np.concatenate(list(output['is_redcell'].values()))
            
        if return_centroids:
            for c_k in centroid_keys:
                output[c_k] = np.concatenate(list(output[c_k].values()))
            
        if return_muse:
            for m_k in muse_keys:
                output[m_k] = np.concatenate(list(output[m_k].values()))

    return output