build:
	jupyter-book clean .
	sed "/[[_TOC_]]/d" README.md > index.md
	jupyter-book build .
	rm index.md

serve:
	python -m http.server 8000 \
		--bind 127.0.0.1 \
		-d _build/html/

