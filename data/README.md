# Data directory

Once you run the steps to download DANDI datasets, the folder `dandi` will appear.

Once you run the `construct-metadata-database.ipynb` notebook, the database file called `database.csv` will be generated.

Once you run the `process-passive-dataset.ipynb` notebook, files will populate `data/proc/{PASSIVE_EXPERIMENT_NAME}` will appear, such as `data/proc/identity`, `data/proc/concentration`.

Once you run the `process-GNG-dataset.ipynb` notebook, files will populate `data/proc/GNG` will appear.

If you run these notebooks in full, this is how it should look in this `data` directory (not showing `.nwb` file; and the per-subject CSV file should depend on which subjects you pick):

```
data
├── dandi
│   └── 000785
│       ├── dandiset.yaml
│       ├── sub-163
│       ├── sub-164
│       ├── sub-2372
│       ├── sub-2400
│       ├── sub-2401
│       ├── sub-2456
│       ├── sub-2457
│       ├── sub-443
│       ├── sub-448
│       ├── sub-512
│       ├── sub-513
│       ├── sub-514
│       ├── sub-533
│       ├── sub-644
│       ├── sub-664
│       ├── sub-668
│       ├── sub-8
│       └── sub-9
├── database.csv
└── proc
    ├── concentration
    │   ├── concentration__percdFF__2457__cell_metadata.csv
    │   ├── concentration__percdFF__2457__fluo_series.csv
    │   ├── concentration__percdFF__2457__trial_metadata.csv
    │   ├── concentration__percdFF.pkl.xz
    │   └── concentration__percdFF.pq.zst
    ├── fast_hz_sniff_trig
    ├── GNG
    │   ├── GNG__cell-metadata.csv
    │   ├── GNG__cell-metadata.pkl.xz
    │   ├── GNG__cell-metadata.pq.zst
    │   ├── GNG__percdFF__0513__cell_metadata.csv
    │   ├── GNG__percdFF__0513__fluo_series.csv
    │   ├── GNG__percdFF__0513__trial_metadata.csv
    │   ├── GNG__percdFF_muse-orc__min_3_sessions__0513__cell_metadata.csv
    │   ├── GNG__percdFF_muse-orc__min_3_sessions__0513__fluo_series.csv
    │   ├── GNG__percdFF_muse-orc__min_3_sessions__0513__trial_metadata.csv
    │   ├── GNG__percdFF_muse-orc__min_3_sessions.pkl.xz
    │   ├── GNG__percdFF_muse-orc__min_3_sessions.pq.zst
    │   ├── GNG__percdFF.pkl.xz
    │   └── GNG__percdFF.pq.zst
    └── identity
        ├── identity__percdFF__0008__cell_metadata.csv
        ├── identity__percdFF__0008__fluo_series.csv
        ├── identity__percdFF__0008__trial_metadata.csv
        ├── identity__percdFF.pkl.xz
        └── identity__percdFF.pq.zst

```
